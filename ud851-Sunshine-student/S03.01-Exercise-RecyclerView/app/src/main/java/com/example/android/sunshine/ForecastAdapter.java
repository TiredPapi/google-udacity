package com.example.android.sunshine;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.zip.Inflater;

/**
 * Created by michal on 11/30/17.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastAdapterViewHolder> {
    private String[] mWeatherData;

    ForecastAdapter() {

    }

    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.forecast_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        return new ForecastAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastAdapterViewHolder holder, int position) {
        String weatherForThisDay = mWeatherData[position];
        holder.mWeatherTextView.setText(weatherForThisDay);
    }

    @Override
    public int getItemCount() {
        if (mWeatherData == null) return 0;
        return mWeatherData.length;
    }

    class ForecastAdapterViewHolder extends RecyclerView.ViewHolder {
        public final TextView mWeatherTextView;

        public ForecastAdapterViewHolder(View itemView) {
            super(itemView);
            mWeatherTextView = (TextView) itemView.findViewById(R.id.tv_weather_data);

        }
    }

    public void setWeatherData(String[] weatherData) {
        mWeatherData = weatherData;
        notifyDataSetChanged();
    }
}



// Within ForecastAdapter.java /////////////////////////////////////////////////////////////////
// Done (15) Add a class file called ForecastAdapter
// Done (22) Extend RecyclerView.Adapter<ForecastAdapter.ForecastAdapterViewHolder>

// Done (23) Create a private string array called mWeatherData

// Done (47) Create the default constructor (we will pass in parameters in a later lesson)

// Done (16) Create a class within ForecastAdapter called ForecastAdapterViewHolder
// Done (17) Extend RecyclerView.ViewHolder
// Within ForecastAdapterViewHolder ///////////////////////////////////////////////////////////
// Done (18)  a public final TextView variable called mWeatherTextView

// Done (19) Create a constructor for this class that accepts a View as a parameter
// Done (20) Call super(view) within the constructor for ForecastAdapterViewHolder
// Done (21) Using view.findViewById, get a reference to this layout's TextView and save it to mWeatherTextView
// Within ForecastAdapterViewHolder ///////////////////////////////////////////////////////////

// Done (24) Override onCreateViewHolder
// Done (25) Within onCreateViewHolder, inflate the list item xml into a view
// Done (26) Within onCreateViewHolder, return a new ForecastAdapterViewHolder with the above view passed in as a parameter

// Dpne (27) Override onBindViewHolder
// Done (28) Set the text of the TextView to the weather for this list item's position

// Done(29) Override getItemCount
// Done (30) Return 0 if mWeatherData is null, or the size of mWeatherData if it is not null

// Done (31) Create a setWeatherData method that saves the weatherData to mWeatherData
// Done (32) After you save mWeatherData, call notifyDataSetChanged
// Within ForecastAdapter.java /////////////////////////////////////////////////////////////////
